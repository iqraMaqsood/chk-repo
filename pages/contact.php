<?php
$nameErr=' ';
$emailErr=' ';
$subErr=' ';
$msgErr=' ';
$checkname=$checkemail=$checksubj=$checkmsg=false;
if(isset($_POST["submit"])){
        if(empty($_POST["name"])){
            $nameErr= "<div>\n\nPlease enter name </div>";
            $checkname=true;
        }       
        else{
            if(!preg_match("/^[a-zA-Z ]*$/",$_POST["name"])){
                $nameErr="<div>Only letters are allowed</div>";
                $checkname=true;
            }
        }
        if(empty($_POST["email"])){
                    $emailErr="<div>Please enter Email</div>";
                    $checkemail=true;   
                }
                else{
                    if(!filter_var($_POST["email"],FILTER_VALIDATE_EMAIL)){
                        $emailErr="<div>Invalid email format</div>";
                        $checkemail=true;
                    }
                }
                 if(empty($_POST["subj"])){
            $subErr= "<div>\n\nPlease enter Subject </div>";
            $checksubj=true;
        }    
         if(empty($_POST["msg"])){
            $msgErr= "<div>\n\nEnter your Message here </div>";
            $checkmsg=true;
        }    
    }
    if($checkname!=true&&$checkemail!=true&&$checksubj!=true&&$checkmsg!=true) {
        include ("config/database.php");
if(isset($_POST['submit'])){

            $id=$_POST['id'];
            $name=$_POST['name'];
            $email=$_POST['email'];
            $subject=$_POST['subj'];
            $message=$_POST['msg'];
            $query=$con->prepare("insert into contact(id,Name,Email,Subject,Message) VALUES(:id,:name,:email,:subject,:message)");
            $query->bindParam(":id",$id);
            $query->bindParam(":name",$name);
            $query->bindParam(":email",$email);
            $query->bindParam(":subject",$subject);
            $query->bindParam(":message",$message);
            $query->execute();
}}
?>
<!doctype html>
<html>
    <head>
      <link href="style.css" rel="stylesheet" type="text/css">
         <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
        <!-- Bootstrap core CSS -->
        <link href="promdb/css/bootstrappro.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="promdb/css/mdbpro.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div>
            <img src="banner2.jpg" class="img-fluid"/>
        </div>
        <!--Navbar-->
        <nav class="navbar navbar-dark" style="background-color:#4B515D;">
            <!--Collapse button-->
            <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#collapseEx2">
                <i class="fa fa-bars"></i>
            </button> 
            <div class="container">
                <!--Collapse content-->
                <div class="collapse navbar-toggleable-xs" id="collapseEx2">
                    <!--Navbar Brand-->
                    <a class="navbar-brand" style="color:white;">Home</a>
                    <!--Links-->
                    <ul class="nav navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link">Jewellery</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link">Features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link">Pricing</a>
                        </li>  
                    </ul>
                    <form style="padding-top:8px; padding-left:12px;" class="form-inline float-lg-right">
                        <a href="" class="nav-link"><i class="fa fa-shopping-cart"  aria-hidden="true"></i>CART</a>
                    </form>
               
                    <form style="padding-top:8px; padding-left:12px; size:13px;" class="form-inline float-lg-right">
                        <a href="" class="nav-link"><i class="fa fa-user" aria-hidden="true">Admin</i></a>
                    </form>
                    <!--Search form-->
                    <form style="padding-left:8px;" class="form-inline float-lg-right">
                        <input class="form-control" type="text" placeholder="Search">
                    </form>
                </div>
                <!--/.Collapse content-->
            </div>
        </nav>
        <!--/.Navbar-->
<form method="POST"  action="" id="frm">
    <div class="container">
<!--Naked Form-->
<div class="card-block">
<input type="hidden"  name="id"/>
    <!--Header-->
    <div class="text-xs-center" >
        <h3><i class="fa fa-envelope"></i> Write to us:</h3>
    </div>
    <div class="md-form">
        <i class="fa fa-user prefix"></i>
        <input type="text" id="form2" class="form-control" name="name">
        <div id="Error"><div><?php echo $nameErr; ?></div></div>
        <label for="form3">Your name</label>
    </div>
    <div class="md-form">
        <i class="fa fa-envelope prefix"></i>
        <input type="text" id="form2" class="form-control" name="email">
        <div id="Error"><?php echo $emailErr; ?></div>
        <label for="form2">Your email</label>
    </div>
    <div class="md-form">
        <i class="fa fa-tag prefix"></i>
        <input type="text" id="form32" class="form-control" name="subj">
        <div id="Error"><?php echo $subErr; ?></div>
        <label for="form34">Subject</label>
    </div>
    <div class="md-form">
        <i class="fa fa-pencil prefix"></i>
        <textarea type="text" id="form8" class="md-textarea" name="msg"></textarea>
         <div id="Error"><?php echo $msgErr; ?></div>
        <label for="form8">Message</label>
    </div>
    <div class="text-xs-center">
        <button class="btn btn-default" id="sub" name="submit" type="submit" onclick="toastr.success('Success');">Submit</button>
    </div>
</div>
</div>
</form>
<!--Naked Form-->
  <!--Footer-->
        <footer class="page-footer center-on-small-only">
            <!--Footer Links-->
            <div class="container-fluid">
                <div class="row">
                    <!--First column-->
                    <div class="col-md-4 offset-md-1">
                        <h5 class="title">Online Jewellery Shop</h5>
                        <p>At Jeweller, we deal in best jewellery products.Since 2000, we have been helping our customers celebrate the milestones in their lives and we have done that by providing our customers with the finest quality jewelry at a good value.</p>
                    </div>
                    <!--/.First column-->
                    <hr class="hidden-md-up">
                    <!--Second column-->
                    <div class="col-md-2 offset-md-1">
                        <h5 class="title">Brands</h5>
                        <ul>
                            <li><a href="piaget.php">Piaget</a></li>
                            <li><a href="damajewel.php">Damas Jewellery</a></li>
                            <li><a href="zevar.php">Zevar</a></li>
                            <li><a href="hanifjewel.php">Hanif Jewellers</a></li>
                        </ul>
                    </div>
                    <!--/.Second column-->
                    <hr class="hidden-md-up">
                     <!--Third column-->
                    <div class="col-md-3">
                        <h5 class="title">Contact Details</h5>
                        <ul>
                            <li>0345-2355367/051-4903532</li>
                            <li>Jewellery.com</li>
                            <li>Islamabad, shop#56 F-10</li>
                            <li>9:00 am to 5:00 pm</li>
                        </ul>
                    </div>
                    <!--/.Third column-->
                </div>
            </div>
            <!--/.Footer Links-->
            <hr>
            <!--Call to action-->
            <div class="call-to-action">
                <ul>
                    <li><h5>Register for free</h5></li>
                    <li><a href="#" class="btn btn-primary">Sign up!</a></li>
                    <li>Already Registered?&nbsp;&nbsp;<a href="#" style="color:#809fff;"><b>Log in</b></a></li>
                </ul>
            </div>
            <!--/.Call to action-->
            <hr>
            <!--Social buttons-->
            <div class="social-section">
                <ul>
                    <li><a href="https://www.facebook.com" class="btn-floating btn-small btn-fb"><i class="fa fa-facebook"> </i></a></li>&nbsp;&nbsp;
                    <li><a href="https://www.twitter.com" class="btn-floating btn-small btn-tw"><i class="fa fa-twitter"> </i></a></li>&nbsp;&nbsp;
                    <li><a href="https://plus.google.com" class="btn-floating btn-small btn-gplus"><i class="fa fa-google-plus"> </i></a></li>&nbsp;&nbsp;
                    <li><a href="https://www.linkedin.com" class="btn-floating btn-small btn-li"><i class="fa fa-linkedin"> </i></a></li>&nbsp;&nbsp;
                    <li><a href="https://github.com" class="btn-floating btn-small btn-git"><i class="fa fa-github"> </i></a></li>&nbsp;&nbsp;
                    <li><a href="https://www.pinterest.com" class="btn-floating btn-small btn-pin"><i class="fa fa-pinterest"> </i></a></li>&nbsp;&nbsp;
                    <li><a href="https://www.instagram.com" class="btn-floating btn-small btn-ins"><i class="fa fa-instagram"> </i></a></li>
                </ul>
            </div>
            <!--/.Social buttons-->
            <!--Copyright-->
            <div class="footer-copyright">
                <div class="container-fluid">
                    © 2016 Copyright: <a href="http://www.MDBootstrap.com"><b> Jewellery.com </b></a>
                </div>
            </div>
            <!--/.Copyright-->
        </footer>
        <!--/.Footer-->
        <!-- JQuery -->
        <script type="text/javascript" src="promdb/js/jquery3.1.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="promdb/js/teether.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="promdb/js/boootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="promdb/js/mddb.min.js"></script>
    </body>
</html> 
