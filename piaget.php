<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" type="text/css" href="styles.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Necklace</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

     <!-- Bootstrap core CSS -->
        <link href="promdb/css/bootstrappro.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="promdb/css/mdbpro.css" rel="stylesheet">
        <!-- Your custom styles (optional) -->
        <link href="css/style.css" rel="stylesheet">
</head>

<body>
        
        <div>
            <img src="banner2.jpg" class="img-fluid"/>
        </div>

        <!--Navbar-->
        <nav class="navbar navbar-dark" style="background-color:#4B515D;">
            <!--Collapse button-->
            <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#collapseEx2">
                <i class="fa fa-bars"></i>
            </button> 
            <div class="container">
                <!--Collapse content-->
                <div class="collapse navbar-toggleable-xs" id="collapseEx2">
                    <!--Navbar Brand-->
                    <a class="navbar-brand" style="color:white;">Home</a>
                    <!--Links-->
                    <ul class="nav navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link">Jewellery</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link">Features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link">Pricing</a>
                        </li>  
                    </ul>
                   
                    <form style="padding-top:8px; padding-left:12px;" class="form-inline float-lg-right">
                        <a href="" class="nav-link"><i class="fa fa-shopping-cart"  aria-hidden="true"></i>CART</a>
                    </form>
               
                    <form style="padding-top:8px; padding-left:12px; size:13px;" class="form-inline float-lg-right">
                        <a href="" class="nav-link"><i class="fa fa-user" aria-hidden="true">Admin</i></a>
                    </form>

                    <!--Search form-->
                    <form style="padding-left:8px;" class="form-inline float-lg-right">
                        <input class="form-control" type="text" placeholder="Search">
                    </form>
                </div>
                <!--/.Collapse content-->
            </div>
        </nav>
        <!--/.Navbar-->
    <!-- Start your project here-->
<!--Section: Products v.3-->
<section class="section" id="products">
    <!--Section heading-->
    <h1 class="section-heading"></h1>
    <!--Section sescription-->
    <p class="section-description"></p>
    <!--First row-->
    <div class="row">
        <!--First column-->
        <div class="col-lg-3 col-md-6 mb-r">
            <!--Card-->
            <div class="card">
                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/1.jpg" class="img-fluid" alt="" id="image">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->
                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>
                   <!--button-->
				<div class="bd-example">
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="Necklace">View Details</button>
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="exampleModalLabel">Necklace</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
            </div>	
<!--showdata-->	
<div class="container">
<figure class="col-md-6">
    <!--Large image-->
   <a  href="necklace/1.jpg" >
        <!-- Thumbnail-->
        <img  id="show" src="necklace/1.jpg" >
    </a>
</figure> 
<div class="data">Code:</div>
<div class="data">Seller:</div>
<div class="data">Condition:</div>
  <!-- Content here -->
</div>
            <div class="md-form"> 
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Order Now</button>
        </div>
      </div>
    </div>
  </div>
</div>
                 <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">59$ <span class="discount">199$</span></span>
                    </div>
                </div>
                <!--/.Card content-->
            </div>
            <!--/.Card-->
        </div>
        <!--/First column-->
        <!--Second column-->
        <div class="col-lg-3 col-md-6 mb-r">
            <!--Card-->
            <div class="card">
                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/7.jpg"  class="img-fluid" alt="" id="image">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->

                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>
				<!--button-->
				<button class="btn btn-primary" type="submit" data-target="#exampleModal" data-whatever="@mdo">View Details</button>
                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">39$ <span class="discount">99$</span></span>
                    </div>
                </div>
                <!--/.Card content-->
            </div>
            <!--/.Card-->
        </div>
        <!--/Second column-->
        <!--Third column-->
        <div class="col-lg-3 col-md-6 mb-r">
            <!--Card-->
            <div class="card">
                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/33.jpg"  class="img-fluid" alt="" id="image">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->
                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>
                   <!--button-->
				<button class="btn btn-primary" type="submit">View Details</button>
                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">79$ <span class="discount">299$</span></span>
                    </div>
                </div>
                <!--/.Card content-->
            </div>
            <!--/.Card-->
        </div>
        <!--/Third column-->
        <!--Fourth column-->
        <div class="col-lg-3 col-md-6 mb-r">
            <!--Card-->
            <div class="card">
                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/4.jpg"  class="img-fluid" alt="" id="image" class="image1">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->
                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>
                   <!--button-->
				<button class="btn btn-primary" type="submit">View Details</button>
                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">79$ <span class="discount">299$</span></span>
                    </div>
                </div>
                <!--/.Card content-->
            </div>
            <!--/.Card-->
        </div>
        <!--/Fourth column-->
    </div>
    <!--/First row-->
</section>
<!--/Section: Products v.3-->
<!--Section: Products v.3-->
<section class="section" id="products">
    <!--First row-->
    <div class="row">
        <!--First column-->
        <div class="col-lg-3 col-md-6 mb-r">
            <!--Card-->
            <div class="card">
                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/5.jpg" class="img-fluid" alt="" id="image">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->
                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>
                   <!--button-->
				<button class="btn btn-primary" type="submit">View Details</button>
                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">59$ <span class="discount">199$</span></span>
                    </div>
                </div>
                <!--/.Card content-->
            </div>
            <!--/.Card-->
        </div>
        <!--/First column-->
        <!--Second column-->
        <div class="col-lg-3 col-md-6 mb-r">
            <!--Card-->
            <div class="card">
                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/6.jpg"  class="img-fluid" alt="" id="image">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->
                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>
				<!--button-->
				<button class="btn btn-primary" type="submit">View Details</button>
                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">39$ <span class="discount">99$</span></span> 
                    </div>
                </div>
                <!--/.Card content-->
            </div>
            <!--/.Card-->
        </div>
        <!--/Second column-->
        <!--Third column-->
        <div class="col-lg-3 col-md-6 mb-r">
            <!--Card-->
            <div class="card">
                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/151.jpg"  class="img-fluid" alt="" id="image">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->
                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>
                   <!--button-->
				<button class="btn btn-primary" type="submit">View Details</button>
                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">79$ <span class="discount">299$</span></span>
                    </div>
                </div>
                <!--/.Card content-->
            </div>
            <!--/.Card-->
        </div>
        <!--/Third column-->
        <!--Fourth column-->
        <div class="col-lg-3 col-md-6 mb-r">
            <!--Card-->
            <div class="card">
                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/8.jpg"  class="img-fluid" alt="" id="image">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->
                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>
                   <!--button-->
				<button class="btn btn-primary" type="submit">View Details</button>
                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">79$ <span class="discount">299$</span></span>
                    </div>
                </div>
                <!--/.Card content-->
            </div>
            <!--/.Card-->
        </div>
        <!--/Fourth column-->
    </div>
    <!--/First row-->

</section>
<!--/Section: Products v.3-->


<!--Section: Products v.3-->
<section class="section" id="products">
    <!--First row-->
    <div class="row">

        <!--First column-->
        <div class="col-lg-3 col-md-6 mb-r">

            <!--Card-->
            <div class="card">

                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/9.jpg" class="img-fluid" alt="" id="image">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->

                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>

                   <!--button-->
				<button class="btn btn-primary" type="submit">View Details</button>

                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">59$ <span class="discount">199$</span></span>
                       
                    </div>

                </div>
                <!--/.Card content-->

            </div>
            <!--/.Card-->

        </div>
        <!--/First column-->

        <!--Second column-->
        <div class="col-lg-3 col-md-6 mb-r">

            <!--Card-->
            <div class="card">

                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/10.jpg"  class="img-fluid" alt="" id="image">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->

                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>

                    

				<!--button-->
				<button class="btn btn-primary" type="submit">View Details</button>



                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">39$ <span class="discount">99$</span></span>
                        
                    </div>

                </div>
                <!--/.Card content-->

            </div>
            <!--/.Card-->

        </div>
        <!--/Second column-->

        <!--Third column-->
        <div class="col-lg-3 col-md-6 mb-r">

            <!--Card-->
            <div class="card">

                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/11.jpg"  class="img-fluid" alt="" id="image">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->

                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>

                   <!--button-->
				<button class="btn btn-primary" type="submit">View Details</button>

                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">79$ <span class="discount">299$</span></span>
                       
                    </div>

                </div>
                <!--/.Card content-->

            </div>
            <!--/.Card-->

        </div>
        <!--/Third column-->

        <!--Fourth column-->
        <div class="col-lg-3 col-md-6 mb-r">

            <!--Card-->
            <div class="card">

                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/12.jpg"  class="img-fluid" alt="" id="image" class="image1">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->

                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>

                   <!--button-->
				<button class="btn btn-primary" type="submit">View Details</button>

                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">79$ <span class="discount">299$</span></span>
                        
                    </div>

                </div>
                <!--/.Card content-->

            </div>
            <!--/.Card-->

        </div>
        <!--/Fourth column-->

    </div>
    <!--/First row-->

</section>
<!--/Section: Products v.3-->


<!--Section: Products v.3-->
<section class="section" id="products">
    <!--First row-->
    <div class="row">

        <!--First column-->
        <div class="col-lg-3 col-md-6 mb-r">

            <!--Card-->
            <div class="card">

                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/13.jpg" class="img-fluid" alt="" id="image">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->

                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>

                   <!--button-->
				<button class="btn btn-primary" type="submit">View Details</button>

                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">59$ <span class="discount">199$</span></span>
                       
                    </div>

                </div>
                <!--/.Card content-->

            </div>
            <!--/.Card-->

        </div>
        <!--/First column-->

        <!--Second column-->
        <div class="col-lg-3 col-md-6 mb-r">

            <!--Card-->
            <div class="card">

                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/14.jpg"  class="img-fluid" alt="" id="image">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->

                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>

                    

				<!--button-->
				<button class="btn btn-primary" type="submit">View Details</button>



                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">39$ <span class="discount">99$</span></span>
                        
                    </div>

                </div>
                <!--/.Card content-->

            </div>
            <!--/.Card-->

        </div>
        <!--/Second column-->

        <!--Third column-->
        <div class="col-lg-3 col-md-6 mb-r">

            <!--Card-->
            <div class="card">

                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/3.jpg"  class="img-fluid" alt="" id="image">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->

                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>

                   <!--button-->
				<button class="btn btn-primary" type="submit">View Details</button>

                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">79$ <span class="discount">299$</span></span>
                       
                    </div>

                </div>
                <!--/.Card content-->

            </div>
            <!--/.Card-->

        </div>
        <!--/Third column-->

        <!--Fourth column-->
        <div class="col-lg-3 col-md-6 mb-r">

            <!--Card-->
            <div class="card">

                <!--Card image-->
                <div class="view overlay hm-white-slight z-depth-1">
                    <img src="necklace/16.jpg"  class="img-fluid" alt="" id="image" class="image1">
                    <a>
                        <div class="mask"></div>
                    </a>
                </div>
                <!--/.Card image-->

                <!--Card content-->
                <div class="card-block text-xs-center">
                    <!--Category & Title-->
                    <a href="" class="text-muted"><h5>Accessories</h5></a>
                    <h4 class="card-title"><strong><a href="">Necklace</a></strong></h4>

                   <!--button-->
				<button class="btn btn-primary" type="submit">View Details</button>

                    <!--Card footer-->
                    <div class="card-footer">
                        <span class="left">79$ <span class="discount">299$</span></span>
                        
                    </div>

                </div>
                <!--/.Card content-->

            </div>
            <!--/.Card-->

        </div>
        <!--/Fourth column-->

    </div>
    <!--/First row-->

</section>
<!--/Section: Products v.3-->
<!--Footer-->
        <footer class="page-footer center-on-small-only">
            <!--Footer Links-->
            <div class="container-fluid">
                <div class="row">
                    <!--First column-->
                    <div class="col-md-4 offset-md-1">
                        <h5 class="title">Online Jewellery Shop</h5>
                        <p>At Jeweller, we deal in best jewellery products.Since 2000, we have been helping our customers celebrate the milestones in their lives and we have done that by providing our customers with the finest quality jewelry at a good value.</p>
                    </div>
                    <!--/.First column-->

                    <hr class="hidden-md-up">

                    <!--Second column-->
                    <div class="col-md-2 offset-md-1">
                        <h5 class="title">Brands</h5>
                        <ul>
                            <li><a href="piaget.php">Piaget</a></li>
                            <li><a href="damajewel.php">Damas Jewellery</a></li>
                            <li><a href="zevar.php">Zevar</a></li>
                            <li><a href="hanifjewel.php">Hanif Jewellers</a></li>
                        </ul>
                    </div>
                    <!--/.Second column-->

                    <hr class="hidden-md-up">

                     <!--Third column-->
                    <div class="col-md-3">
                        <h5 class="title">Contact Details</h5>
                        <ul>
                            <li>0345-2355367/051-4903532</li>
                            <li>Jewellery.com</li>
                            <li>Islamabad, shop#56 F-10</li>
                            <li>9:00 am to 5:00 pm</li>
    </div>
</div>
                        </ul>
                    </div>
                    <!--/.Third column-->
                </div>
            </div>
            <!--/.Footer Links-->

            <hr>

            <!--Call to action-->
            <div class="call-to-action">
                <ul>
                    <li><h5>Register for free</h5></li>
                    <li><a href="#" class="btn btn-primary">Sign up!</a></li>
                    <li>Already Registered?&nbsp;&nbsp;<a href="#" style="color:#809fff;"><b>Log in</b></a></li>
                </ul>
            </div>
            <!--/.Call to action-->

            <hr>

            <!--Social buttons-->
            <div class="social-section">
                <ul>
                    <li><a href="https://www.facebook.com" class="btn-floating btn-small btn-fb"><i class="fa fa-facebook"> </i></a></li>&nbsp;&nbsp;
                    <li><a href="https://www.twitter.com" class="btn-floating btn-small btn-tw"><i class="fa fa-twitter"> </i></a></li>&nbsp;&nbsp;
                    <li><a href="https://plus.google.com" class="btn-floating btn-small btn-gplus"><i class="fa fa-google-plus"> </i></a></li>&nbsp;&nbsp;
                    <li><a href="https://www.linkedin.com" class="btn-floating btn-small btn-li"><i class="fa fa-linkedin"> </i></a></li>&nbsp;&nbsp;
                    <li><a href="https://github.com" class="btn-floating btn-small btn-git"><i class="fa fa-github"> </i></a></li>&nbsp;&nbsp;
                    <li><a href="https://www.pinterest.com" class="btn-floating btn-small btn-pin"><i class="fa fa-pinterest"> </i></a></li>&nbsp;&nbsp;
                    <li><a href="https://www.instagram.com" class="btn-floating btn-small btn-ins"><i class="fa fa-instagram"> </i></a></li>
                </ul>
            </div>
            <!--/.Social buttons-->

            <!--Copyright-->
            <div class="footer-copyright">
                <div class="container-fluid">
                    © 2016 Copyright: <a href="http://www.MDBootstrap.com"><b> Jewellery.com </b></a>

                </div>
            </div>
            <!--/.Copyright-->

        </footer>
        <!--/.Footer-->

<!-- /Start your project here-->
    <!-- SCRIPTS -->
     <!-- JQuery -->
    <script type="text/javascript" src="promdb/js/jquery3.1.min.js"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="promdb/js/teether.min.js"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="promdb/js/boootstrap.min.js"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="promdb/js/mddb.min.js"></script>
    
           <script type="text/javascript">
		   
            $('#exampleModal').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.modal-title').text('' + recipient)
                modal.find('.modal-body input').val(recipient)
            })
			// MDB Lightbox Init
$(function () {
    $("#mdb-lightbox-ui").load("mdb-addons/mdb-lightbox-ui.html");
});
        </script>
</body>
</html>